angular.module('tpfotos')
 .service('fotosHttpSvc', function($q, $http) {
   
        this.getsetlist = function() {
      var setlist = null;
      return $http({
          method: 'GET',
          url: 'https://www.flickr.com/services/rest',
          params: {
              method: 'flickr.photosets.getList',
              api_key: 'b1360e735bd2f35eaca4629bc2aad1ce',
                user_id: '151192543@N04',
              format: 'json',
              nojsoncallback: 1
          }
      })
        .then(function(respuesta) {
          
          return _.cloneDeep(respuesta.data.photosets.photoset);
          
        });
    };
    
       this.getfotos = function(id) {
      var fotos = null;
      return $http({
          method: 'GET',
          url: 'https://www.flickr.com/services/rest',
          params: {
              method: 'flickr.photosets.getPhotos',
              photoset_id: id,
              api_key: 'b1360e735bd2f35eaca4629bc2aad1ce',
                user_id: '151192543@N04',
              format: 'json',
              nojsoncallback: 1
          }
      })
        .then(function(respuesta) {
          return _.cloneDeep(respuesta.data.photoset);
        });
    };
  
      this.getfoto = function(id) {
      
      return $http({
          method: 'GET',
          url: 'https://www.flickr.com/services/rest',
          params: {
              method: 'flickr.photos.comments.getList',
              photo_id: id,
              api_key: 'b1360e735bd2f35eaca4629bc2aad1ce',
                user_id: '151192543@N04',
              format: 'json',
              nojsoncallback: 1
          }
      })
        .then(function(respuesta) {
          return _.cloneDeep(respuesta.data.comments);
        });
    };
    
    /*this.getfecha = function(id){
    return $http({
          method: 'GET',
          url: 'https://www.flickr.com/services/rest',
          params: {
              method: 'flickr.photos.getInfo',
              photo_id: id,
              api_key: 'b1360e735bd2f35eaca4629bc2aad1ce',
                
              format: 'json',
              nojsoncallback: 1
          }
      })
        .then(function(respuesta) {
          return _.cloneDeep(respuesta.data.photo.dates.taken);
        });
    }*/
    

})
  .service('fotosDbSvc', function($q, $ionicPlatform) {
    var db = null;

    //Abre la base de datos y crea las tablas si aun no existen
    
    $ionicPlatform.ready(function() {
      db = window.sqlitePlugin.openDatabase({name: "albunes.db", location: 'default'}, function() {
      db.transaction(function(tx) {
        tx.executeSql("CREATE TABLE IF NOT EXISTS albun(id text primary key, title text)",[], 
                      function(tx, result) {
            alert("tabla creada");
            
        },
                      function (error){
            alert("tabla no creada");
        });
      }, db.close )
    });
        });
    
    this.getsetlist = function() {
      return $q(function(resolve, reject) {
        db.executeSql("SELECT * FROM albun", [],
          function(resultado) {

            resolve(rows(resultado));
          },
          reject);
      });
    };
      this.actualizarAlbun = function(setlist) {
      var sqlStatments = [ "DELETE FROM albun" ];
      setlist.forEach(function(setlist) {
          //  alert(JSON.stringify(setlist.id, null, 2));
        //    alert(JSON.stringify(setlist.title, null, 2));

        sqlStatments.push([ "INSERT INTO albun(id, title) VALUES(?, ?)", [ setlist.id, setlist.title._content] ]);
      });

      return $q(function(resolve, reject) {
        db.sqlBatch(sqlStatments, resolve, reject);
      });
    };
        function rows(resultado) {
      var items = [];
      for (var i = 0; i < resultado.rows.length; i++) {
        items.push(resultado.rows.item(i));
                               // alert(JSON.stringify(resultado.rows.item(i), null, 2));

      }
      return items;
    }
})

.service('fotosSvc', function($q, fotosHttpSvc, fotosDbSvc, conexion) {
    var setlist = null;
              

    this.getsetlist = function() {
      if (conexion.online()) {
                              // alert("entro online")
         

        return fotosHttpSvc.getsetlist()
          .then(function(respuesta) { setlist = respuesta 
                         /*            if(setlist==null || setlist==''){
                    alert("conexion offline")
                            return fotosDbSvc.getsetlist();
                }*/
                                    })
          .then(function() { fotosDbSvc.actualizarAlbun(setlist); })
          .then(function() {
          //  alert(JSON.stringify(setlist, null, 2));

            
            //return fotosDbSvc.getsetlist();

            return _.cloneDeep(setlist);
          });
      } else {
                    alert("no hay conexion=offline")

        return fotosDbSvc.getsetlist();
      }
    };
    this.getfotos = function(id) {
      if (!conexion.online()) {
        return $q.reject('Sin conexión');
      }
      return fotosHttpSvc.getfoto(id);
    };    
  });