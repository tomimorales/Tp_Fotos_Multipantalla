angular.module('tpfotos', ['ionic','chart.js'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('app', {
      url: '/app',
      abstract: true,    
      templateUrl: 'templates/menu.html',
      controller: 'AppCtrl'
    })
    .state('app.bienvenido', {
      url: '/bienvenido',
      views: {
        'menuContent': {
          templateUrl: 'templates/bienvenido.html'
        }
      }
    })
.state('app.galeria', {
      url: '/galeria/:id',
      views: {
        'menuContent': {
          templateUrl: 'templates/galeria.html',
          controller: 'galeriaController'
        }
      }
    })
  .state('app.foto', {
      url: '/foto/:farm/:server/:id/:secret',
      views: {
        'menuContent': {
          templateUrl: 'templates/foto.html',
          controller: 'fotoController'
        }
      }
    })
  ;
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/bienvenido');
})
.config(function($httpProvider) { //hay q hacerlo pq no esta en la misma aplicacion por eso se bloquea solo para browser
    $httpProvider.defaults.useXDomain = true;
    $httpProvider.defaults.headers.common = 'Content-Type: application/json';
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
  }
);
