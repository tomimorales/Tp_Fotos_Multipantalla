
angular.module('tpfotos')
  .controller('fotoController', function($scope, $state, $stateParams, $ionicLoading, fotosHttpSvc) {

    

    //Cargar el albun
    if ($stateParams.id) {
      showIonicLoading()
        .then(obtenerComentarios)
      .then(function(comentarios){
          $scope.comentarios= comentarios;
          $scope.farm= $stateParams.farm
            $scope.server= $stateParams.server
              $scope.id= $stateParams.id
              $scope.secret= $stateParams.secret
        
      })
        .then($ionicLoading.hide)
        .catch($ionicLoading.hide);
       
    }
    
    var options = {
    message: 'share this', // not supported on some apps (Facebook, Instagram)
    subject: 'the subject', // fi. for email
    files: ['', ''], // an array of filenames either locally or remotely
    url: 'https://www.website.com/foo/#bar?a=b',
    chooserTitle: 'Pick an app' // Android only, you can override the default share sheet title
        }

       var onSuccess = function(result) {
    console.log("Share completed? " + result.completed); // On Android apps mostly return false even while it's true
    console.log("Shared to app: " + result.app); // On Android result.app is currently empty. On iOS it's empty when sharing is cancelled (result.completed=false)
       }

       var onError = function(msg) {
    console.log("Sharing failed with message: " + msg);
       }

       window.plugins.socialsharing.shareWithOptions(options, onSuccess, onError);
    

        $scope.enviarpormail = function(x){window.plugins.socialsharing.shareViaEmail(
            'Message', // can contain HTML tags, but support on Android is rather limited:  http://stackoverflow.com/questions/15136480/how-to-send-html-content-with-image-through-android-default-email-client
    'Subject',
    ['juan_ratablank@hotmail.com'], // TO: must be null or an array
    null, // CC: must be null or an array
    null, // BCC: must be null or an array
    ['x',x], // FILES: can be null, a string, or an array
    onSuccess, // called when sharing worked, but also when the user cancelled sharing via email. On iOS, the callbacks' boolean result parameter is true when sharing worked, false if cancelled. On Android, this parameter is always true so it can't be used). See section "Notes about the successCallback" below.
    onError // called when sh*t hits the fan
        )}
       
    
    function showIonicLoading() {
      return $ionicLoading.show({
        template: '<ion-spinner icon="lines"/>'
      });
    }
      function obtenerComentarios() {
      return fotosHttpSvc.getfoto($stateParams.id);
    }
});