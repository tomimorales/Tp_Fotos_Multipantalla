* Nos surgio problema con el servicio conexion (setfotos.servicios), ya que cuando se efectua el control para saber si el estado es online u offline, siempre entra por el true. 
Sin embargo pudimos comprobar que la bd sqlLite funciona bien, ya que efectuamos alert con los valores que son devueltos(luego de efectuar select) y estos son los correctos.
A esto ultimo lo comprobamos ejecutando la funcion (return fotosDbSvc.getsetlist()), se encuentra comentado.

* No realizamos la implimentacion de la ordenacion de las fotos por fechas, ya que el json devuelto utilizado para la carga de fotos, no contiene este valor.
Para solucionarlo debemos hacer una peticion al servicio gteinfo y crear un vector con estos resultados. Esto solucion nos sumo mucha complejidad

* No pudimos probar la opcion de enviar url por mail, si bien esta instalado el plugin, y los agoritmos están, no pudimos probar la aplicacion en un celular, por lo tanto no comprobamos que ande dicha opcion

* Esperamos tener una respuesta para solucionar los inconvenientes de los plugin de sqlite y de envio de mail

Integrantes: Zamora Eric - Guzman Juan - Morales Tomás